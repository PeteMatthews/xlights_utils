#!/usr/bin/php
<?php

$h = $argv[1];
$w = $argv[2];
$s = $argv[3];

$t = 25;
$total = 0;

echo "Height ".$h.", Width ".$w.", Spacing ".$s."\n\n";

// Width first - Available space is width - trunking

function gen_stats($label, $l, $s)
{
    global $t;
    $l = $l - $t;
    $gaps = ceil($l / $s);
    $s = $l / $gaps;
    $pixels = $gaps + 1;
    echo $label.": ".$pixels." pixels , spaced at ".$s."\n";
    $a = Array();
    for ($i = 0; $i < $pixels; $i++)
    {
        $a[] = round((($i * $s) + ($t / 2)), 2);
    }
    return $a;
}

$a = gen_stats("Width", $w, $s);
foreach ($a as $v)
{
    echo $v." ";
}
echo "\n\n";
$total += count($a) * 2;

$a = gen_Stats("Height", $h, $s);
// Drop the first one, adjust all the others by trunking width, drop the last
echo "Vert trunking needs to be ".($h - ($t * 2))."\n";
for ($i = 1; $i < count($a) - 1; $i++)
{
    echo ($a[$i] - $t)." ";
}
echo "\n\n";
$total += (count($a) - 2) * 2;

echo "Total ".$total." pixels\n\n";

