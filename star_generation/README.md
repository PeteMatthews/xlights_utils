# How to use this thing

This is not a great guide. It's a difficult thing to describe. I should probably draw a bunch of images to go with it. If you want to try it out, grab a piece of A4 and try a 21cm tall star with 30 pixels total (rings of 20 and 10). It'll make more sense when you're doing it. Probably.

## You will need:
* A square-ish piece of material - Coro, plywood, paper, whatever
* A good ruler
* Sharpie

## You will need a rough idea of:
* How tall you want the star to be - The way the maths works, the stars are slightly wider than they are tall, so the height should max out at the short-side of the material you have.
* How many pixels you want in concentric rings - They'll want to be multiples of 10. My stars are (50, 30, 10) and (40, 20)

## Run the thing

Run the tool, as follows:

    ./calc.php [height] [max ring] [min ring] [step]

For mine on a 100cm tall board, something like:

    ./calc.php 100 50 10 20

You'll get something that looks like:

    pete@dalaran:~/xlights_utils/star_generation$ ./calc.php 100 50 10 20
    Height 100, Width 105.15
    Center 44.72 high, 52.57 across
    Points on bounding box:
      Baseline: 20.08, 85.07
      Sides:    61.8
    Pixel gap 6.69
    
     Ext: Spine Lengths:  55.28,  21.11
      50: Spine Lengths:  46.07,  17.60
      30: Spine Lengths:  27.64,  10.56
      10: Spine Lengths:   9.21,   3.52
    
    Pixels at 0 6.69 13.39 20.08 26.77 33.47
    
    Total pixels : 90 (~5.4A PSU)

## What does this tell you?

### Firstly, some star positioning:

    Height 100, Width 105.15
    Center 44.72 high, 52.57 across

So your material is 100cm tall by 105.15cm wide. Go draw that box on your material. Make sure the corners are properly square.

The center of the star is not directly in the middle of this rectangle - It's central horizontally (52.57cm from each side), but only 44.72cm from the base of the rectangle. Go mark that as a point on your material.

### Next, the construction of the star points:
    Points on bounding box:
      Baseline: 20.08, 85.07
      Sides:    61.8

When you draw your star, it will have 5 points. 

- One of these will be in the center of the top edge of your material.
- 2 will be on the bottom edge
- 1 will be on each side edge (2 total)

Mark the one at the center of the top edge. It should be directly above the center that you marked already.

On the bottom edge, mark 2 points, one 20.08cm from the left hand edge, and one 85.07cm from the left hand edge (or, you'll notice, 20.08cm from the right hand edge).

On the sides, measure up 61.8 from the bottom edge and mark.

Draw a bunch of lines connecting these all up. You should wind up with a nice even star. You'll also want to draw lines from the center to each point. When you draw these, extend them past the center, to meet the inner corners of the star. 

### Now for the concentric bits:

     Ext: Spine Lengths:  55.28,  21.11
      50: Spine Lengths:  46.07,  17.60
      30: Spine Lengths:  27.64,  10.56
      10: Spine Lengths:   9.21,   3.52

So, each concentric star (including the exterior outline itself) will have 2 important measurements. The distance from the center of the star to the point, and the distance from the center to the inner corner.

You should notice that in our example, measuring from the star points to the center should be 55.28cm, and from the inner corners to the center is 21.11cm.

So, on each line from the center to the points, mark the longer spine lengths, measuring from the center:

* 46.07 from the center
* 27.64 from the center
* 9.21 from the center

And on each line from the center to the corners, mark the shorter spine lengths:

* 17.60 from the center
* 10.56 from the center
* 3.52 from the center

You should now have all the points for your concentric stars. Go ahead and join each one up. You should wind up with a bunch of nice even concentric stars.

### Nearly done

Now we just need to plot the pixel points on the lines. 

    Pixels at 0 6.69 13.39 20.08 26.77 33.47

Along each star edge, mark points at these measurements. Clearly, on the smaller stars, you won't mark them all, but that's fine. We're basically marking points 6.69cm apart.

Drill out all those holes. 

