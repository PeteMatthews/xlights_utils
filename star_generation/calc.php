#!/usr/bin/php
<?php

function cosd($a)
{
    return cos(deg2rad($a));
}

function sind($a)
{
    return sin(deg2rad($a));
}

function tand($a)
{
    return tan(deg2rad($a));
}

function ro($n)
{
    return round($n, 2);
}

if ($argc < 4)
{
    echo "Usage: $argv[0] [height] [max_pixel_ring] [min_pixel_ring] [step]\n";
    echo "e.g. $argv[0] 100 50 10\n";
    echo "\n";
    exit(1);
}

if ($argc == 4)
{
    $step = 10;
}
else
{
    $step = $argv[4];
}

$max_ring = $argv[2] / 10;
$min_ring = $argv[3] / 10;

$height = $argv[1];
$spine = $height / (1 + cosd(36));
$width = 2 * $spine * sind(72);

$outer_ring = $max_ring + 1;

$outer_edge = $spine / (cosd(18) + (sind(18) / tand(36)));
$pixel_gap = $outer_edge / $outer_ring;

$short_spine = ($outer_edge * sind(18)) / sind(36);

$baseline_offset = $spine * sind(36);
$side_offset = $spine * sind(18);

echo "Height ".$height.", Width ".ro($width)."\n";
echo "Center ".ro($height - $spine)." high, ".ro($width / 2)." across\n";
echo "Points on bounding box:\n";
echo "  Baseline: ".ro(($width / 2) - $baseline_offset).", ".ro(($width / 2) + $baseline_offset)."\n";
echo "  Sides:    ".ro(($height - $spine) + $side_offset)."\n";
echo "Pixel gap ".ro($pixel_gap)."\n\n";

printf(" Ext: Spine Lengths: %6.2f, %6.2f\n", $spine, $short_spine);

$total_pixels = 0;
$pixel_pos = "";

for ($i = $max_ring; $i >= $min_ring; $i -= ($step / 10))
{
    $edge_len = $i * $pixel_gap;
    $l_spine = $edge_len * (cosd(18) + (sind(18) / tand(36)));
    $s_spine = ($edge_len * (sind(18) / tand(36))) / cosd(36);
    printf("%4d: Spine Lengths: %6.2f, %6.2f\n", $i*10, $l_spine, $s_spine);
    if ($pixel_pos == "")
    {
        for ($j = 0; $j <= $i; $j++)
        {
            $pixel_pos .= ro($j * $pixel_gap) . " ";
        }
    }
    $total_pixels += $i * 10;
}
echo "\nPixels at ".$pixel_pos."\n";
echo "\nTotal pixels : ".$total_pixels." (~".ro($total_pixels * 0.06)."A PSU)\n";
